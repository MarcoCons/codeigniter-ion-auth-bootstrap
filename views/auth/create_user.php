<div class="container my-5">
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <form class="card box-shadow shadow-sm" action="<?=base_url().'auth/create_user'?>" method="post">
        <div class="card-body">
          <h1 class="text-center card-title"><?=lang('create_user_heading')?></h1>
          <p class="text-center card-text"><?=lang('create_user_subheading')?></p>
<?php if(isset($message)): ?>
          <div class="alert alert-info my-3" id="infoMessage">
            <?=$message?>
          </div>
<?php endif; ?>
          <!-- First name -->
          <div class="form-group">
            <label for="first_name"><?=lang('create_user_fname_label', 'first_name')?> <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="first_name" id="first_name" value="<?=$first_name['value']?>" required>
          </div>
          <!-- Last name -->
          <div class="form-group">
            <label for="last_name"><?=lang('create_user_lname_label', 'last_name')?> <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="last_name" id="last_name" value="<?=$last_name['value']?>" required>
          </div>
          <!-- Email -->
          <div class="form-group">
            <label for="email"><?=lang('create_user_email_label', 'email')?> <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="email" id="email" value="<?=$email['value']?>" required>
          </div>
<?php if($identity_column !== 'email'): ?>
          <!-- Identity -->
          <div class="form-group">
            <label for="identity"><?=lang('create_user_identity_label', 'identity')?> <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="identity" id="identity" value="<?=$identity['value']?>" required>
            <?=form_error('identity', '<div class="invalid-feedback">', '</div>')?>
          </div>
<?php endif; ?>

          <!-- Company -->
          <!--
          <div class="form-group">
            <label for="company"><?php //ang('create_user_company_label', 'company')?></label>
            <input class="form-control" type="text" name="company" id="company" value="<?php//$company?>">
          </div>
          -->
          <!-- Phone number -->
          <!--
          <div class="form-group">
            <label for="phone"><?php//lang('create_user_phone_label', 'phone')?></label>
            <input class="form-control" type="tel" name="phone" id="phone" value="<?php//$phone?>">
          </div>
          -->
          <!-- Password -->
          <div class="form-group">
            <label for="password"><?=lang('create_user_password_label', 'password')?>  <span class="text-danger">*</span></label>
            <input class="form-control" type="password" name="password" id="password" required>
          </div>
          <!-- Confirm password -->
          <div class="form-group">
            <label for="password_confirm"><?=lang('create_user_password_confirm_label', 'password_confirm')?>  <span class="text-danger">*</span></label>
            <input class="form-control" type="password" name="password_confirm" id="password_confirm" required>
          </div>
          <div class="text-center mt-3">
            <button type="submit" class="btn btn-primary"><?=lang('create_user_submit_btn')?></button>
          </div>
        </div><!-- .card-body -->
      </form>
    </div><!-- .col -->
    <div class="col-md-3"></div>
  </div><!-- .row -->
</div><!-- .container -->
