<div class="container my-5">
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col">
      <form class="card box-shadow shadow-sm" action="<?=base_url()?>auth/login" method="post">
        <div class="card-body">
          <h1 class="text-center card-title"><?=lang('login_heading')?></h1>
          <p class="text-center card-text"><?=lang('login_subheading')?></p>
<?php if(isset($message) && !empty($message)): ?>
          <div class="alert alert-info my-3" id="infoMessage">
            <?=$message?>
          </div>
<?php endif; ?>
          <!-- Identity -->
          <div class="form-group">
            <label for="identity"><?=lang('login_identity_label', 'identity')?></label>
            <input class="form-control" type="text" name="identity" id="identity" value="">
          </div>
          <!-- Password -->
          <div class="form-group">
            <label for="password"><?=lang('login_password_label', 'password')?></label>
            <input class="form-control" type="password" name="password" id="password" value="">
          </div>
          <!-- Remember me -->
          <div class="d-flex justify-content-between">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="1" id="remember" name="remember">
              <label class="" for="remember"><?=lang('login_remember_label', 'remember')?></label>
            </div>
            <p class="text-right">
              <a href="forgot_password"><?=lang('login_forgot_password')?></a>
            </p>
          </div>
            <div class="text-center mt-3">
              <button type="submit" class="btn btn-primary"><?=lang('login_submit_btn')?></button>
            </div>
        </div><!-- .card-body -->
      </form>
    </div><!-- .col -->
    <div class="col-md-3"></div>
  </div><!-- .row -->
</div><!-- .container -->
